package com.horse.mvvmpractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.horse.mvvmpractice.database.AppDatabase;
import com.horse.mvvmpractice.model.Person;
import com.horse.mvvmpractice.utils.AppExecutors;

import java.util.List;

public class BasicRoomOperationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_room_operation);

        doSomeRoomOperation();;
    }

    private void doSomeRoomOperation() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                AppDatabase appDb = AppDatabase.getInstance(BasicRoomOperationActivity.this, "person_db");

                //Inserting new Person
                appDb.personDao().insertPerson(new Person("Jassi", "Delhi"));

                //Fetching from Person
                List<Person> people = appDb.personDao().getPersonList();
            }
        });
    }
}
