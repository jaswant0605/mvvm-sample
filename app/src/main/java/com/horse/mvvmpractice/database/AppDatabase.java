package com.horse.mvvmpractice.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.horse.mvvmpractice.dao.MovieDao;
import com.horse.mvvmpractice.dao.PersonDao;
import com.horse.mvvmpractice.model.Person;

@Database(entities = Person.class, exportSchema = false, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context, String dbName) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, dbName)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract PersonDao personDao();
    public abstract MovieDao movieDao();
}
