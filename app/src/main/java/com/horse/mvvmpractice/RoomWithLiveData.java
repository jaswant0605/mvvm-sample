package com.horse.mvvmpractice;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.horse.mvvmpractice.database.AppDatabase;
import com.horse.mvvmpractice.model.Movie;
import com.horse.mvvmpractice.repositories.MovieRepository;
import com.horse.mvvmpractice.viewmodel.MovieViewModel;

import java.util.List;

public class RoomWithLiveData extends AppCompatActivity {

    private MovieViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_with_live_data);

        doDatabaseOperation();
    }

    private void doDatabaseOperation() {
        viewModel = ViewModelProviders.of(this).get(MovieViewModel.class);
        viewModel.getAllMovies().observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(@Nullable List<Movie> movies) {

            }
        });
        //viewModel.getA
    }
}
