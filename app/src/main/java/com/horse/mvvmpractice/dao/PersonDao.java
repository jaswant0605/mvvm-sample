package com.horse.mvvmpractice.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.horse.mvvmpractice.model.Person;

import java.util.List;

@Dao
public interface PersonDao {
    @Query("Select * from person")
    List<Person> getPersonList();

    @Insert
    void insertPerson(Person person);

    @Update
    void updatePerson(Person person);

    @Delete
    void delecePerson(Person person);

}
